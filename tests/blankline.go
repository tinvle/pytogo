//# Test handling of blank line in mid-function.
func me() {
    fmt.Print(2)

    fmt.Print(3)
}

func you() {
    fmt.Print(5)
    fmt.Print(6)
}

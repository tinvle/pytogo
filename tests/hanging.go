//# Verify correct processing of hanging indents.
        for revision, record := range self.revisions {
            for _, node := range record.nodes {
                if node.fromPath != nil {
                    copynodes = append(copynodes, node)
                    announce(debugEXTRACT, fmt.Sprintf("copynode at %s", node))
                }
                if node.action == sdADD || node.kind == sdDIR {
                    for _, trial := range global_options["svn_branchify"] {
                        if "*" !in trial || trial == node.path {
                            self.branches[node.path+os.sep] = nil
                        } else if trial.endswith(os.sep + "*")
                                 || filepath.Dir(trial)
                                 || node.path + os.sep + "*" !in options {
                            self.branches[node.path+os.sep] = nil
                        } else if trial == "*" {
                            self.branches[node.path+os.sep] = nil
                        }
                    }
                    if node.path+os.sep in self.branches {
                        announce(debugSHOUT, "recognized as a branch)
                    }
                }
            }
            // Per-commit spinner disabled because this pass is fast
            //baton.twirl("")
        }
        copynodes.sort(key=operator.attrgetter("fromRev"))

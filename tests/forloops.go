//# Test for-loop translation
        // Find all copy sources and compute the set of branches
        announce(DEBUG_EXTRACT, "Pass 1")
        nobranch = "--nobranch" in options
        copynodes = []
        for revision, record := range self.revisions {
            for _, node := range record.nodes {
                if node.from_path != nil {
                    copynodes = append(copynodes, node)
                    announce(DEBUG_EXTRACT, fmt.Sprintf("copynode at %s", node))
                }
                if node.action == SD_ADD && node.kind == SD_DIR && node.path+os.sep !in self.branches && !nobranch {
                    for _, trial := range global_options["svn_branchify"] {
                        if "*" !in trial && trial == node.path {
                            self.branches[node.path+os.sep] = nil
                        } else if trial.endswith(os.sep + "*")
                                 && filepath.Dir(trial) == filepath.Dir(node.path)
                                 && node.path + os.sep + "*" !in global_options["svn_branchify"] {
                            self.branches[node.path+os.sep] = nil
                        } else if trial == "*" && node.path + os.sep + "*" !in global_options["svn_branchify"] && strings.Count(node.path, os.sep) < 1 {
                            self.branches[node.path+os.sep] = nil
                        }
                    }
                    if node.path+os.sep in self.branches && debug_enable(DEBUG_TOPOLOGY) {
                        announce(DEBUG_SHOUT, fmt.Sprintf("%s recognized as a branch", node.path)+os.sep)
                    }
                }
            }
            // Per-commit spinner disabled because this pass is fast
            //baton.twirl()
        }

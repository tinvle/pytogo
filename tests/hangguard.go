//# Test that in case of a hanging conditional the opening bracket lands rught.
        // This guard filters out the empty
        // nodes produced by format 7 dumps.
        if !(node.action == SD_CHANGE
                && node.props == nil
                && node.blob == nil
                && node.from_rev == nil) {
            nodes = append(nodes, node)
        }
        node = nil
